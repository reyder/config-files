##Nginx install instruction

* Download the NGINX 1.9.x tar-gzip package. Either visit http://nginx.org/en/download.html, or run:
```
$ wget http://nginx.org/download/nginx-1.11.1.tar.gz
```

* Unpack the tar-gzip package:
```
$ tar xvfz nginx-1.11.1.tar.gz
$ cd nginx-1.11.1
```

* ~~Download and apply the HTTP/2 module patch from http://nginx.org/patches/http2/:~~ `Merged with new version`
```
$ wget http://nginx.org/patches/http2/patch.http2.txt
$ patch -p1 < patch.http2.txt
```

* Configure NGINX (WOOX) build:
```
$ ./configure --prefix=/usr/local/nginx --sbin-path=/usr/local/nginx/sbin/nginx --conf-path=/usr/local/nginx/conf/nginx.conf --pid-path=/usr/local/nginx/nginx.pid --lock-path=/usr/local/nginx/logs/nginx.lock --error-log-path=/usr/local/nginx/logs/error.log --http-log-path=/usr/local/nginx/logs/access.log --user=nginx --group=nginx --with-ipv6 --with-http_ssl_module --with-http_geoip_module --with-http_v2_module --add-module=/tmp/ngx_pagespeed-master
```

* Build NGINX:
```
$ make & make install
```
