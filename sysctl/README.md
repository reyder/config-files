##Kernel sysctl configuration file for Linux

```
Version 1.12 - 2015-09-30
Michiel Klaver - IT Professional
http://klaver.it/linux/ for the latest version - http://klaver.it/bsd/ for a BSD variant

This file should be saved as /etc/sysctl.conf and can be activated using the command:
sysctl -e -p /etc/sysctl.conf

For binary values, 0 is disabled, 1 is enabled.  See sysctl(8) and sysctl.conf(5) for more details.

Tested with:
Ubuntu 14.04 LTS kernel version 3.13
Debian 7 kernel version 3.2
CentOS 7 kernel version 3.10
```

###Credits
```
http://www.enigma.id.au/linux_tuning.txt
http://www.securityfocus.com/infocus/1729
http://fasterdata.es.net/TCP-tuning/linux.html
http://fedorahosted.org/ktune/browser/sysctl.ktune
http://www.cymru.com/Documents/ip-stack-tuning.html
http://www.kernel.org/doc/Documentation/networking/ip-sysctl.txt
http://www.frozentux.net/ipsysctl-tutorial/chunkyhtml/index.html
http://knol.google.com/k/linux-performance-tuning-and-measurement
http://www.cyberciti.biz/faq/linux-kernel-tuning-virtual-memory-subsystem/
http://www.redbooks.ibm.com/abstracts/REDP4285.html
http://www.speedguide.net/read_articles.php?id=121
http://lartc.org/howto/lartc.kernel.obscure.html
http://en.wikipedia.org/wiki/Sysctl
```
